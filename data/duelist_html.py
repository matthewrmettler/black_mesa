import glob
import operator
import sys

callers = ["Ajia", "Amanda", "Apple", "Sadie", "Claire", "Harris", 
"Napoleon", "Misty", "Lacey", "Shadow", "Peaches", "Kheldar", "Rena"]
file_pre = "parsed_matches/"
file_post = "/*.txt"
move_names = ["Jab", "Hook", "Uppercut", "Chop", "Jumpkick", "Spinkick", "Snapkick", "Sweep", "Flip", "ArmBlock", "Legblock", "Dodge", "Duck", "Leap"]
short_move_names = ["JB", "HK", "UC", "CH", "JK", "SP", "SK", "LS", "FL", "AB", "LB", "DO", "DU", "LP"]
duelists = []
matches = []
round_move_count = [dict() for x in range(15)]

class Round:
	def __init__(self, result):
		#print(result)
		self.number = result[0]
		self.winner_move = replace(result[1])
		self.loser_move = replace(result[2])
		self.winner_score = result[3]
		self.loser_score = result[4]

	def toString(self):
		print("Round " + self.number + ": " + self.winner_move + " and " + self.loser_move)
class Match:
	def __init__(self, lines):
		#print(lines)
		self.winner = lines[0].rstrip('\n')
		self.loser = lines[1].rstrip('\n')
		r = lines[2:]
		rounds = []
		for line in r:
			#print(line)
			result = line.split(" ")
			if (len(result) != 5): pass 
			rounds.append(Round(result))
		self.rounds = rounds

	def toString(self):
		print(self.winner.replace("\n", "") + " vs " + self.loser)
		for r in self.rounds:
			#r.toString()
			pass
class Duelist:
	def __init__(self, name):
		self.name = name

	def details(self):
		print(self.name)

def replace(string):
	if string in move_names:
		return string
	elif string in short_move_names:
		return move_names[short_move_names.index(string)]
	elif string[1:] in short_move_names:
	 	return move_names[short_move_names.index(string[1:])]
 	elif string[2:] in short_move_names:
 		return "Feint " + move_names[short_move_names.index(string[2:])]
	elif string[5:] in move_names: #w/o fancy
		return string[5:]
	else:
		#print("PANIC DUDE " + string)
		if string.lower() == "SN".lower() or string.lower() == "SNK".lower(): 
			return "Snapkick"
		return string
def get_following_moves(duelist):
	#todo: store. just printing for now
	for match in matches:	
		duelist_moves = []
		opponent_moves = []
		match.toString()
		if match.winner == duelist:
			print(duelist + " is match winner")
			for round in match.rounds:
				duelist_moves.append(round.winner_move)
				opponent_moves.append(round.loser_move)
		else:
			print(duelist + " is match loser")
			for round in match.rounds:
				duelist_moves.append(round.loser_move)
				opponent_moves.append(round.winner_move)
		for i in range(len(opponent_moves)-1):
			print("{0} followed his opponent's {1} with a {2}".format(duelist, opponent_moves[i], duelist_moves[i+1] ))
def get_followed_self_moves(duelist, move):
	results = {}
	for match in matches:	
		duelist_moves = []
		#match.toString()
		if match.winner == duelist:
			#print(duelist + " is match winner")
			for round in match.rounds:
				duelist_moves.append(round.winner_move)
		else:
			#print(duelist + " is match loser")
			for round in match.rounds:
				duelist_moves.append(round.loser_move)
		for i in range(len(duelist_moves)-1):
			if duelist_moves[i] == move:
				if not duelist_moves[i+1] in results:
					results[duelist_moves[i+1]] = 1
				else:
					results[duelist_moves[i+1]] += 1
	results = sorted(results.iteritems(), key=operator.itemgetter(1), reverse = True)
	print_followed_self(duelist, move, results)

def get_followed_moves(duelist, move):
	results = {}
	for match in matches:	
		duelist_moves = []
		opponent_moves = []
		#match.toString()
		if match.winner == duelist:
			#print(duelist + " is match winner")
			for round in match.rounds:
				duelist_moves.append(round.winner_move)
				opponent_moves.append(round.loser_move)
		else:
			#print(duelist + " is match loser")
			for round in match.rounds:
				duelist_moves.append(round.loser_move)
				opponent_moves.append(round.winner_move)
		for i in range(len(opponent_moves)-1):
			if opponent_moves[i] == move:
				if not duelist_moves[i+1] in results:
					results[duelist_moves[i+1]] = 1
				else:
					results[duelist_moves[i+1]] += 1
	results = sorted(results.iteritems(), key=operator.itemgetter(1), reverse = True)
	print_followed(duelist, move, results)
def print_followed(duelist, move, results):
	#print(results)
	print("{0} followed his opponent's {1} with a:".format(duelist, move))
	if (len(results) == 0):
		print("(No data for this move)")
	for r in results:
		print("{0} x{1}".format(r[0], r[1]))
	print("")

def print_followed_self(duelist, move, results):
	#print(results)
	print(duelist + " followed his own {0} with:".format(move))
	if (len(results) == 0):
		print("(No data for this move)")
	for r in results:
		print("{0} x{1}".format(r[0], r[1]))
	print("")

def get_following_self_moves(duelist):
	#todo: store. just printing for now
	for match in matches:	
		duelist_moves = []
		#match.toString()
		if match.winner == duelist:
			#print(duelist + " is match winner")
			for round in match.rounds:
				duelist_moves.append(round.winner_move)
		else:
			#print(duelist + " is match loser")
			for round in match.rounds:
				duelist_moves.append(round.loser_move)
		for i in range(len(duelist_moves)-1):
			#print(duelist + " followed his own " + duelist_moves[i] + " with a " + duelist_moves[i+1])
			pass
def get_round_count(duelist):
	print("List of moves made by {0} by round:".format(duelist))
	#print(round_move_count)
	for match in matches:
		duelist_moves = []
		if match.winner == duelist:
			#print(duelist + " is match winner")
			for round in match.rounds:
				duelist_moves.append(round.winner_move)
		else:
			#print(duelist + " is match loser")
			for round in match.rounds:
				duelist_moves.append(round.loser_move)
		for i in range(len(duelist_moves)):
			#print(i)
			#print(duelist_moves[i])
			#print(round_move_count)
			if not duelist_moves[i] in round_move_count[i]:
				round_move_count[i][duelist_moves[i]] = 1
			else:
				round_move_count[i][duelist_moves[i]] += 1
	for i in range(len(round_move_count)):
		round_move_count[i] = sorted(round_move_count[i].iteritems(), key=operator.itemgetter(1), reverse = True)
	for i in range(len(round_move_count)):
		print("Round {0}: ".format(i+1) + str(round_move_count[i]))

def main(duelist):
	duelist_matches = []
	for c in callers:
	 	for f in glob.iglob(file_pre + c + file_post):
			if duelist in f:
				duelist_matches.append(f)
	for duel in duelist_matches:
		with open(duel, "r") as match:
			matches.append(Match(list(match)))
	#print(len(matches))
	#get_following_moves(duelist)
	#get_following_self_moves(duelist)
	get_round_count(duelist)
	for m in move_names:
		get_followed_moves(duelist, m)
		get_followed_self_moves(duelist, m)

def getMoveCount(matches, move):
	count = 0
	for m in matches:
		#print(m.toString())
		for r in m.rounds:
			if r.winner_move == move: count += 1
			if r.loser_move == move: count += 1

def main_noduelist():
	raw_matches = []
	matches = []
	duelists = {}
	for c in callers:
	 	for f in glob.iglob(file_pre + c + file_post):
	 		raw_matches.append(f)
	print("There have been " + str(len(raw_matches)) + " matches")
	for duel in raw_matches:
		with open(duel, "r") as match:
			matches.append(Match(list(match)))
	for m in matches:
		if m.winner not in duelists:
			duelists[m.winner] = 1
		else:
			duelists[m.winner] += 1
		if m.loser not in duelists:
			duelists[m.loser] = 1
		else:
			duelists[m.loser] += 1
	duelists = sorted(duelists.iteritems(), key=operator.itemgetter(1), reverse = True)
	print("There are {0} duelists".format(len(duelists)))
	for d in duelists:
		print("{0}, {1} matches ({2}% of matches)".format(d[0], d[1], "%.2f" % (float(d[1])/float(len(raw_matches))*100.0)))
	#print(len(matches))
	for m in move_names:
		getMoveCount(matches, m)

#main("Kyra")
main_noduelist()
#make it so that you can do 'what did ___ follow his own Flip with'