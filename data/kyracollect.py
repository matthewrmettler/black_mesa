import glob


kyra_matches = []

def hasKyra(filename):
	with open(filename, 'r') as f:
		for line in f:
			if 'kyra' in line.lower():
				return True
	return False

def main():
	for filename in glob.iglob('match cleaned/*.txt'):
		 if hasKyra(filename):
		 	kyra_matches.append(filename)
 	print(kyra_matches)

main()