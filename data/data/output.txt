Farseer
1 (Arm Block / Jab. 0+-0, Warren.)
2 (Sweep / Dodge. 1-0, Kyra.)
3 (Jab / Snapkick. 1-1.)
4 (Sweep / Chop. 2-1, Kyra.)
5 (Snapkick / Diuck. 2-2.)
6 (Jab / Sweep. 3-2, Kyra.)
7 (Uppercut / Flip. 3-3.)
8 (Jab / Jab. 4-4.)
F (Snapkick / Sweep. 5-4, Kyra.)
( (Kyra Jessup .def. Eternal Quest, 5-4 in 9, DoF.**)


DARK
M ( Jab / sweep ) 1 Kyra
M ( Flip / Jumpkick ) 2 Kyra
M ( Jab x2 ) 3-1 Kyra
M ( Sweep / Jumpkick ) 3-1 Kyra
M ( Flip / Sweep ) 3-2 Kyra
M ( Chop / Flip ) 3-3
M ( Jab x2 ) 4-4
M ( Snap / Sweep )
( ( Kyra Jessup .def. Mister Collins, 5-4 in 8 DOF )


Hydra
1 (Jab / Snapkick. 1-0, E.)
2 (Chop / Leap. 2-0, E.)
3 (Dodge / Snapkick. 2+-0, E.)
4 (Sweep / Flip. 2-1, E.)
5 (Sweep / Flip. 3-1, E.)
6 (Duck / Hook. 3-1+, E.)
7 (Arm Block / Leap. 3-1, E.)
8 (Jab / Jab. 4-2, E.)
9 (Sweep / Uppercut. 4-2, E.)
1 (Jab / Sweep. 4-4.)
F (Jumpkick / Snapkick. 5-4, Mister Collins.)
( (Mister Collins .def. Ekthbjgke, 5-4 in 11, DoF.**)


Palamecia
1 (JB/LS) 1 Crunchem
2 (CH/SN) 2 Crunchem
3 (JB/FL) 2-1 Crunchem
4 (LS/LS) 3-2 Crunchem
5 (SP/DO) 4-2 Crunchem
6 (DO/SN) 4+-2 Crunchem
7 (LS/CH)
( ((Final: Crunchem .def. Mister Collins, 5-2 in 7. DoF))


Happy
T (Fancy Duck/Jab) 1-0 Blue
T (Snapkick/Sweep) 1 up!
T (Jab/Sweep) 2-1 Blue
T (Jab/Chop) 3-2 Blue
T (Snapkick/Flip) 4-2 Blue
T (Jab/Snapkick) 5-2 Blue
( ((Blue .def. .gallowscalibrator, 5-2 in 6 rounds))


Hammerhead
1 (Jab / Spinkick. 1-0, Grace.)
2 (Sweep / Fancy Dodge. 1-1.)
2 (Snapkick / Spinkick. 2-1, Skid.)
4 (Jab / Jab. 3-2, Skid.)
5 (Sweep / Jumpkick. 3-2, Skid.)
6 (Jab / Spinkick. 3-3.)
7 (Sweep / Chop. 4-3, Skid.)
8 (Fancy Dodge / Jab. 4-4.)
F (Flip / Jab. 5-4, Skid.)
( (Necromesh .def. Awkward, 5-4 in 9, DoF.**)


Dirt
1 (LS/UC) 1 Kyra
2 (HK/FL) 1 All
3 (CH/JK) 1-2 Warren
4 (JB/LS) 2 All
5 (FAB/SN) 3-2 Kyra
6 (DO/JB) 3+-2 Kyra
7 (AB/HK) 4-2 Kyra
8 (LS/LS)
( ((Final: Kyra Jessup .def. eternal quest, 5-3 in 8. DoF))


Big
I (Jab/Sweep) 1-0 Mr. Collins
I (Flip/Jab) 2-0 Mr. Collins
I (Sweep/Dodge) 3-0 Mr. Collins
I (Arm Block/Snapkick) 3-0+ Mr. Collins
I (Jab/Sweep) 3-1 Mr. Collins
I (Snapkick/Flip) 4-1 Mr. Collins
I (Sweep/Duck) 5-1 Mr. Collins
( ((Mister Collins .def. Et Malleus, 5-1 in 7 rounds))


Rending
1 (Sweep / Dodge. 1-0, Sandy.)
2 (Snapkick / Flip. 2-0, Sandy.)
3 (Fandy Dodge / Jab. 3-0, Sandy.)
4 (Jab / Snapkick. 4-0, Sandy.)
5 (Jumpkick / Snapkick. 4-1, Sandy.)
6 (Sweep / Uppercut. 4-2, Sandy.)
7 (Fancy Dodge / Leap. 4-2, Sandy.)
F (Jab / Jab. 5-3, Sandy.)
( (Sandalio del Saenz .def. Brimstone, 5-3 in 8, DoF.**)


RE
1 (HC/TH) 1 - 0 Taneth
2 (TH/LC) 1 All
3 (SS/CP) 1.5 - 1.0 Eri
4 (LP/TH) 2 - 1
5 (LC/HC) 3 - 2 Eri
6 (HC/TH) 3 All
7 (CP/LC) 3.5 - 3.0 Eri
8 (LC/DI) 4.5 - 3.0 Eri
9 (SHx2) 4.5 - 3.0 Eri
1 (SS/TH) 5 - 3 Eri
( (( FINAL: Sukeban .def. Taneth Mercer, 5 - 3 in 10. ))


Somethin'
B (LS/LS) 1 All
B (SP/FL) 2-1 Sal
B (LS/JB) 2 All
B (JK/FDO) 2-3 Kyra
B (DO/DU) 2-3 Kyra
B (FL/JB) 3 All
B (LS/SN) 3-4 Kyra
B (SN/CH)
( ((Final: Kyra Jessup .def. Delahada, 5-3 in 8. DoF))


Roller
C (JB/LS) 1 Crunchem-who-is-not-a-lady-I-guess
C (CH/JK) 1 ALl
C (JB/LS) 2-1 Crunchem
C (AB/JB) 2+-1 Crunchem
C (LS/AB) 3-1 Crunchem
C (FL/FL) 3-1 Crunchem
C (CH/JB) 4-2 Crunchem
C (LS/FL)
( ((Final: Crunchem .def. Ekthbjlgke, 5-2 in 8. DoF))


Maybe!
O (Jab/Sweep) 1-0 Blue
T (Jab/Chop) 2-1 Blue
T (Jumpkick/Leap) 3-1 Blue
F (Fancy Dodge/Duck) Still 3-1 Blue
F (Dodge/Jab) 3-1+ Blue
S (Jab/Jumpkick) 3-2 Blue
S (Snapkick/Sweep) 3 all!
E (Fancy Dodge/Jab) 4-3 Blue
N (Dodge/Snapkick) 4-3+ Blue
T (Arm Block/Chop) 4 all!
E (Flip/Jab) 5-4 Toni
( ((Et Malleus .def. Blue, 5-4 in 11 rounds))


Crisp
1 (JB/JB) 1 all!
3 (CH/LS) 2 all!


D
S (Sweep/Jab, 1-0 Cookie)
H (Chop/Duck, 1 all)
S (Uppercut/Dodge, 1+-1 Cookie)
B (Jab/Sweep, 2-1 Warren)
W (Sweep/Hook, 3-1 Warren)
W (Legblock/Leap, 3-1 Warren)
A (Jumpkick x2, 3-1 Warren)
O (Spinkick x2, 4-2 Warren)
W (Flip/Sweep, 4-3 Warren)
A (Snapkick/Uppercut, 5-3 Warren)
( ((Final: eternal quest .def. Little Cookie, 5-3 in 10 rounds, DoF))


Anything
G (Wizard Blades/Fear Touch) 1-0 Greenlee
G (Fear Touch/Mage Bolt) 2-0 Greenlee
G (Wizard Blades/Fear Touch) 3-0 Greenlee
G (Mage Bolt/Wizard Blades) 3.5-0 Greenlee
G (Mind Whip x2) Still 3.5-0 Greenlee
G (Ghostform/Wizard Blades) 4-0 Greenlee
G (Reflection/Mage Bolt) 5-0 Greenlee
( ((Greenlee .def. The Boy, 5-0 in 7 rounds))


H
O (Jab/Leap, 1-0 Elaine)
C (Uppercut/Snapkick, 1 all)
O (Sweep/Jumpkick, 1 all)
B (Jab/Armblock, 1+-1 Cookie)
U (Chop/Spinkick, 2-1 Elaine)
C (Jab/Dodge, 2-1+ Elaine)
A (Sweep/Hook, 3-1 Elaine)
E (Flip/Jab, 4-1 Elaine)
O (Jumpkick/Snapkick, 5-1 Elaine)
( ((Final: Elaine Aqua .def. Little Cookie, 5-1 in 9 rounds, DoF))


L
O (FA Spinkick/Flip, 1-0 Queen)
S (Hook/Jab, 1 all)
D (Jab/Sweep, 2-1 Queen)
S (Chop/FA Dodge, 2 all)
D (FA Dodge/Leap, 2 all)
S (Flip/Snapkick, 3-2 Sandy)
S (Sweep/FA Dodge, 3 all)
O (Flip/Jab, 4-3 Queen)
O (Uppercut/Leap, 5-3 Queen)
( ((Final: Queen .def. Sandalio del Saenz, 5-3 in 9 rounds, DoF))


Candy
S (Fancy Dodge/Jab) 1-0 Blue
S (Chop/Snapkick) 1 up!
S (Jab/Sweep) 2-1 Blue
S (Chop/Spinkick) 3-1 Blue
S (Fancy Dodge/Jab) 4-1 Blue
S (Snapkick/Uppercut) 4-2 Blue
S (Flip/Jab) 4-3 Blue
S (Sweep/Chop) 4 up!
S (Mentor Fancy Duck/Jab) 5-4 Blue
( ((Blue .def. Necromesh, 5-4 in 9 rounds))


Patch
A (th x2) 1 all
A (lc/hc) 2 all
A (ss/sh)
A (sh/th) 3-2 Blue
A (hc x2)
A (th/sh) 3 all
A (lc/lp) 4-3 Blue
A (hc/lc)
( ((Blue def Munchem, 5-4 in 8))


SLASH
1 (SL/HC) 1-0 Rhi
2 (SS/LC) 2-0 Rh
3 (TH/TH) 3-1 Rhi
( (FINAL: Awkward .def. Ekthbjlgke, 4-1 in 6 (forfeit), DOF)
4 (HC/LC) 4-2 Rhi
5 (TH/FDU) 4-3 Rhi
6 (CP/LC) 4-3.5 Rhi
7 (LC/TH) 5-3.5 Rhi
( (FINAL: Rhiannon D. Harker .def. edie rolstein, 5-3.5 in 7, DOS


Protera
1 (FDU/LS) 1 Warren
2 (SN/SN) 1-2 Warren
3 (LS/JK) 1-2 Warren
4 (JB/JB) 2-3 Warren
5 (SN/FL) 3 All
6 (JB/CH) 4 All
7 (LS/JB)
( ((Final: eternal quest .def. Blue, 5-4 in 7. DoF))


GALAXY
Q ( Sweep / Flip ) 1 Mr. C
Q ( Flip / Spinkick ) 1-1
( ((Final: Kyra Jessup .def. Zynn, 5-3 in 7. DoF))
Q ( Jab / FDoge ) 1-2 Grace
Q ( Flip / Jumpkick ) 2-2
Q ( Jab / Chop ) 3-3
Q ( Snapkick x2 ) 4-4
Q ( Sweep / FDodge )
( ( Mister Collins .def. Awkward, 5-4 in 7 DOF )
Q ( Jab x2 ) 1-1
Q ( Sweep / Armblock ) 2-1 Blue
Q ( FDodge / Sweep ) 2-2
Q ( F Duck / Flip ) 3-2 Blue
Q ( jab x2 ) 4-3 Blue
Q ( Snap / Jumpkick ) 4-4
Q ( Jab x2 ) 5-5
Q ( Sweep x2 ) 6-6
Q ( [Mentor] FA Dodge / Chop )
( ( Blue .def. Ekthbjlgke, 7-6 in 9 DOF )


Supreme
1 (TH/HC) 1 Gren
2 (LC/LC) 1 Gren
3 (SH/FDU) 1 Gren
4 (LC/FSS) 2 Gren
5 (FSS/HC) 2-1 Gren
6 (FCP/HC) 3-1 Gren
( (FCP/LC)
7 (FLP/FSS) 3-2 Gren
8 (TH/LC) 4-2 Gren
9 (FSS/FDU) 4-2 Gren
F (TH/TH)
( ((Final: Gren Blockman .def. Morgan le Fay, 5-3 in 10.))


"Em.


Over
i (CH/JB) 1 all!


HONEY
1 (sorry Sal) (DO/FDO) 0-0
2 (JB/FDU) 1-0 Mel
3 (LS/JB) 2-0 Mel
4 (SP/FDO) 2-1 Mel
5 (LS/LS) 3-2 Mel
6 (AB/JB) 3-2+ Mel
7 (he's kinky), jabbing while Mel fakes a snapkick! (JB/Ft SN) 3 all
7 (SN/LS) 4-3 Sal
9 (DO/FDU) 4-3 Sal
1 (JB/JB) 5-4 Sal
( (FINAL: Delahada .def. Fourth, 5-3 in 10, DOF)


STICK


EMERALD
1 (FDO/JB) 1-0 Candy
2 (JK/Fa LP) 2-0 Candy
3 (FDO/Ft JK) 2-1 Candy
4 (FL/SN) 2 all
5 (LS/FDO) 3-2 CAndy
6 (JB/FAB) 3 all!
7 (C: LS/G: JK) 3 all!
8 (JB/JB) 4 all!
9 (C: ft SN/G: SN) 5-4 Gren
( (FINAL: Gren Blockman .def. Candy Hart, 5-4 in 9, DOF)


Specter
1 (Jab / Spinkick. 1-0, Blue.)
2 (Flip / Fancy Arm Block. 1-1.)
3 (Jab / Spinkick. 2-1, Blue.)
4 (Sweep / Flip. 2-2.)
5 (Fancy Duck / Flip. 3-2, Blue.)
6 (Jab / Uppercut. 4-2, Blue.)
F (Uppercut / Leap. 5-2, Blue.)
( (Light Up The Night .def Night Trouble, 5-2 in 7, DoF**)


Riptide
1 (Snapkick / Flip. 1-0, Addie.)
2 (Jab / Hook. 2-0, Addie.)
3 (Snapkick / Snapkick. 3-1, Addie.)
4 (Sweep / Flip. 3-2, Addie.)
5 (Chop / Jab. 4-3, Addie.)
6 (Jab / Snapkick. 4-4.)
F (Sweep / Spinkick. 5-4, Addie.)
( (Addie Alcar .def. Eternal Quest, 5-4 in 7, DoF**)


Siren
1 (JB/JB) 1 ALl
2 (JK/FDO) 1-2 Munchem
3 (JB/JB) 2-3 Munchem
4 (SN/SN) 3-4 Munchem
5 (FDO/CH) 4 All
6 (FDU/SN)
( ((Final: Munchem .def. Awkward, 5-4 in 6. DoF))


Way
B (Dodge/Jab) 0+-0 Izumi
B (Leap/Sweep) 1-0 Izumi
B (Chop/Snapkick) 2-0 Izumi
B (Flip/Jab) 3-0 Izumi
B (Sweep/Flip) 4-0 Izumi
B (Fancy Dodge/Dodge) Still 4-0 Izumi
B (Arm Block/Snapkick) 4+-0 Izumi
B (Jab/Sweep) 4-1 Izumi
B (Snapkick/Flip) 5-1 Izumi
( ((Izumi Takamine .def. Kyra Jessup, 5-1 in 9 rounds))


Aladdin
1 (fcp/th) 1-0 Mel
2 (fss/lc) 1 all
3 (fdk/flp) 2-1 Mel
4 (lc/fss) 3-1 Mel
5 (th/hc) 4-1 Mel
6 (fcp/fdk) 4-2 Mel
7 (th/sh) 4-3 Mel
8 (lc/sl)
( ((Fourth def Ekthbjlgke, 5-3 in 8))


Lone
S (Fancy Duck/Jab) 1-0 Kruger
S (Sweep/Flip) 2-0 Kruger
S (Snapkick/Sweep) 3-0 Kruger
S (Jab/Sweep) 3-1 Kruger
S (Jab/Jumpkick) 4-1 Kruger
S (Sweep/Chop) 5-1 Kruger
( ((Kruger .def. Mister Collins, 5-1 in 6 rounds))


Butcher
P (Dodge/Flip) 0+-0 Greenlee
P (Sweep/Arm Block) 1-0 Boy
P (Dodge/Snapkick) 1-0+ Boy
P (Jab/Sweep) 1 all!
P (Jab/Sweep) 2-1 Boy
P (Arm Block/Dodge) Still 2-1 Boy
P (Flip/Jab) 2 even!
P (Jab/Jumpkick) 3-2 Greenlee
P (Sweep/Chop) 3 up!
P (Jab/Snapkick) 4-3 Greenlee
P (Jab/Sweep) 4 all!
P (Spinkick/Dodge) 5-4 Boy
( ((The Boy .def. Greenlee, 5-4 in 12 rounds))


Rebellion
1 (Snapkick / Flip. 1-0, Kyra.)
2 (Chop / Jab. 2-1, Kyra.)
3 (Sweep / Flip. 3-1, Kyra.)
4 (Jab / Sweep. 4-1, Kyra.)
5 (Snapkick / Sweep. 4-2, Kyra.)
F (Jab / Jab. 5-3, Kyra.)
( (Kyra Jessup .def. Mister Collins, 5-3 in 6, DoF.**)


Pulse
1 (DP/MW) .5 Les
2 (WB/GF) .5 All
3 (FMW/WB) 2-.5 Les
4 (GF/SH) 2-.5 Les
5 (RF/AB) 2-1.5 Les
6 (MW/AR) 3-1.5 Les
7 (WB/MW) 3-2.5 Les
8 (DP/FT) 3.5-2.5 Les
9 (SH/FF) 4-2.5 Les
1 (MB/MW)
( ((Final: Lesinda .def. Mister Collins, 5-2.5 in 10. DoM))


Dreadnought
1 (Leap / Sweep. 0+-0, Sandy.)
( (Recall round 8 Snk/AB)
2 (Dodge / Flip. 1-0, Sandy.)
3 (Jab / Jab. 2-1, Sandy.)
4 (Snapkick / Snapkick. 3-2, Sandy.)
5 (Fancy Dodge / Duck. 3-2, Sandy.)
6 (Snapkick / Flip. 4-2, Sandy.)
F (Fancy Dodge / Jab. 5-2, Sandy.)
( (Sandalio del Saenz .def. Ekthbjlgke, 5-2 in 7, DoF.)


CRISP
2 (JK/CH) 2-1 Grace
3 (JB/DO) 2-1+ Grace
5 (SP/DU) 2+-2 Sal
6 (LS/AB) 3-2 Grace
7 (FDO/DO) 3-2 Grace!
8 (JB/JB) 4-3 Grace
9 (LS/LS) 5-4 Grace!
( (FINAL: Awkward .def. Delahada, 5-4 in 9, DOF)


"In


THIRD
S ( LP / TH ) .5 JC
S ( DU / LP ) 1 JC
S ( SH / CP ) 1 JC
S ( CP / TH ) 1.5 Jin
S ( HC / SH ) 2.5 Jin
S ( FSS / SS ) 2.5 Jin
S ( CP / LC ) 3 Jin
S ( SH x2 ) 3 Jin
S ( SL / LP ) 4 Jin
S ( FDIS / LC ) 4-1 Jin
S ( SH / SS ) 4-1 Jin
S ( HC x2 ) 4-1 Jin
S ( SS / TH ) 4.5-1 Jin
S ( TH / CP ) 4.5-1.5 Jin
S ( HC / LC )
( ( JC .def. Sukeban, 5.5 - 2.5 in 15 )


Carbuncle
1 (JB/JB) 1 All
3 (CH/FL) 2-3 Zynn
4 (JB/JB) 3-4 Zynn
5 (JK/SN) 4 All
6 (FDU/FL)
( ((Final: Blue .def. Zynn, 5-4 in 6. DoF))


Smoke!
O (Sweep/Flip) 1-0 Warren
T (Jab/Chop) 2-1 Warren
T (Snapkick/Hook) 2 up!
F (Sweep/Arm Block) 3-2 Warren
F (Jab/Chop) 4-3 Warren
S (Dodge/Uppercut) 4-3+ Warren
S (Duck/Spinkick) 4 all!
E (Flip/Jab) 5-4 Matthew
( ((Matthew Davidson .def. eternal quest, 5-4 in 8 rounds))


C
O (Dodge/Jab, 0+-0 Munch)
O (Armblock/Dodge, 0 all)
U (Flip/Snapkick, 1-0 Sandy)
O (Jab/Flip, 2-0 Sandy)
D (Sweep/FA Dodge, 2-1 Sandy)
S (Jab x2, 3-2 Sandy)
T (Sweep/Chop, 3 all for Munch and Sandy)
O (Jumpkick/FA Dodge, 4-3 Sandy)
O (Jab/Snapkick, 4 all for Munch and Sandy) SUDDEN DEATH!!


Ring:
I (JK/SnK) 1 all (<a href="https://www.youtube.com/watch?v=MzCLLHscMOw)" target="_blank" style="color:#660066">https://www.youtube.com/watch?v=MzCLLHscMOw)</a>
I (FDO/SW) 1-2 The Boy.... come on everyone! Stop and Stare! ( <a href="https://www.youtube.com/watch?v=HtNS1afUOnE" target="_blank" style="color:#660066">https://www.youtube.com/watch?v=HtNS1afUOnE</a> )
I (JB/SnK) 2 all!! I'm giving all my Secrets away! ( <a href="https://www.youtube.com/watch?v=qHm9MG9xw1o" target="_blank" style="color:#660066">https://www.youtube.com/watch?v=qHm9MG9xw1o</a> )
I (JK/JB) 2-3 The Boy! ( <a href="https://www.youtube.com/watch?v=ZSM3w1v-A_Y" target="_blank" style="color:#660066">https://www.youtube.com/watch?v=ZSM3w1v-A_Y</a> )
I (FDO/AB) 2-3 The Boy ( <a href="https://www.youtube.com/watch?v=TOrnUquxtwA" target="_blank" style="color:#660066">https://www.youtube.com/watch?v=TOrnUquxtwA</a> )
I (<a href="https://www.youtube.com/watch?v=UR1r6NCeoaM)" target="_blank" style="color:#660066">https://www.youtube.com/watch?v=UR1r6NCeoaM)</a> (SW/JB) 2-4 The Boy
I ( <a href="https://www.youtube.com/watch?v=__bOEU_XUBw" target="_blank" style="color:#660066">https://www.youtube.com/watch?v=__bOEU_XUBw</a> ) (SnK/JB) 2-4+ The Boy
I (JK/Du) Does anyone else need more cowbell? ( <a href="https://www.youtube.com/watch?v=UfD9fWt8hrQ" target="_blank" style="color:#660066">https://www.youtube.com/watch?v=UfD9fWt8hrQ</a> )
( (The Boy .def. Munchem, 5-2 in 9, DoF)


G
K (Sweep/Dodge, 1-0 Kane)
S (Snapkick/Jab, 1 all)
K (Dodge/Snapkick, 1+-1 Kane)
O (Leap/FA Dodge, 1 all for Kane/Sandy)
S (Jab/Flip, 2-1 Sandy)
S (Jumpkick/Jab, 3-1 Sandy)
C (Armblock/FA Leap, 3-1 Sandy)
B (Snapkick x2, 4-2 Sandy)
S (Uppercut/FA Dodge, 5-2 Sandy)
( ((Final: Sandalio del Saenz .def. Kane, 5-2 in 9 rounds, DoF))


Broadside
1 (Chop / Jab. 1-1.)
2 (Snapkick / Sweep. 2-1, Kane.)
3 (Jab / Chop. 3-2, Kane.)
4 (Legblock / Sweep. 3-2+, Kane.)
5 (Flip / Arm Block. 4-2, Kane.)
F (Jab / Jab. 5-3, Kane.)
( (Brimstone .def. Kyra Jessup, 5-3 in 6, DoF.)


K
T (Flip/Legblock, 0 all)
O (Jab/Snapkick, 1-0 Kimone)
C (Flip/Sweep, 1 all)
C (Chop/Jumpkick, 2-1 Cookie)
K (Jab/Hook, 2 all)
K (Sweep/Dodge, 3-2 Kimone)
A (Jab/Leap, 4-2 Kimone)
D (Flip/Legblock, 4-2 Kimone)
M (Sweep x2, 5-3 Kimone)
( ((Final: Kimone Kidd .def. Little Cookie, 5-3 in 9 rounds, DoF))


Work
1 (JB/JB) 1 All
2 (SN/JK) 1-2 E
3 (JB/FL) 1-3 E
4 (FAB/LS) 1-4 E
5 (FL/JB) 2-4 E
6 (JK/CH) 3-4 E
7 (FL/LS)
( ((Final: Ekthbjlgke .def. Kyra Jessup, 5-3 in 7. DoF))


Anima
1 (JB/JB) 1 All
2 (SN/LS) 2-1 Kyra
3 (DO/JB) 2-1 Kyra
4 (AB/FL) 2 All
5 (JB/LS) 3-2 Kyra
6 (CH/JB) 4-3 Kyra
7 (SN/HK)


*DO/JB



Swing
D (Jab/Sweep) 1-0 E
D (Spinkick/Flip) 1 all!
D (Sweep/Fancy Duck) 2-1 E
D (Jab x2) 3-2 E
D (Fancy Dodge/Jumpkick) 3 up!
D (Jab/Snapkick) 4-3 Kimone
D (Sweep/Flip) 4 up!
D (Hook/Fancy Dodge) 5-4 E
( ((Ekthbjlgke .def. Kimone Kidd, 5-4 in 8 rounds))


Fern!
O (Jab x2) 1 all!
T (Sweep x2) 2 up!
T (Fancy Dodge/Flip) 3-2 Kyra
F (Jab x2) 4-3 Kyra
F (Jumpkick/Snapkick) 4 even!
S (Duck/Jab) 4+-4 Carlyle
S (Flip/Arm Block) 5-4 Kyra
( ((Kyra Jessup .def. Forever Carlyle, 5-4 in 7 rounds))

Fern2!
O (Jab/Sweep) 1-0 Carlyle
T (Snapkick/Sweep) 1 even!
T (Leap/Sweep) 1+-1 Carlyle
F (Duck/Spinkick) 2-1 Carlyle
F (Fancy Dodge/Jab) 2 even!
S (Fancy Arm Block/Chop) 3-2 Jewell
S (Flip/Jab) 3 up!
E (Snapkick x2) 4 all!
N (Flip/Jab) 5-4 Carlyle
( ((Forever Carlyle .def. JewellRavenlock, 5-4 in 9 rounds))

Fern3!
O (Jab/Sweep) 1-0 Mr. Collins
T (Jab/Jumpkick) 1 up!
T (Chop/Snapkick) 2-1 Carlyle
F (Sweep/Flip) 2 even!
F (Jab x2) 3 all!
S (Dodge/Jumpkick) 3+-3 Carlyle
S (Duck/Dodge) Still 3 all
E (Snapkick/Flip) 4-3 Carlyle
N (Leap/Snapkick) 4+-3 Carlyle
T (Duck/Jab) 5-3 Carlyle
( ((Forever Carlyle .def. Mister Collins, 5-3 in 10 rounds))

Fern4!
O (Flip/Jab) 1-0 Crunchem
T (Dodge/Snapkick) 1+-0 Crunchem
T (Jab x2) 2-1 Crunchem
F (Jumpkick/Arm Block) 2+-1 Crunchem
F (Sweep x2) 3-2 Crunchem
S (Spinkick/Flip) 4-2 Crunchem
S (Jab/Sweep) 4-3 Crunchem
E (Duck/Leap) Still 4-3 Crunchem
N (Jab x2) 5-4 Crunchem
( ((Crunchem .def. Ekthbjlgke, 5-4 in 9 rounds))

Fern5!
O (Jab/Sweep) 1-0 Carlyle
T (Dodge x2) Still 1-0 Carlyle
T (Jab/Sweep) 1 up!
F (Flip/Chop) 2-1 Carlyle
F (Sweep/Arm Block) 2 up!
S (Jab/Sweep) 3-2 E
S (Chop/Snapkick) 4-2 E
E (Spinkick/Dodge) 5-2 E
( ((Ekthbjlgke .def. Forever Carlyle, 5-2 in 8 rounds))


Johnny
G (Fancy Duck/Jab) 1-0 Blue
G (Sweep/Fancy Dodge) 2-0 Blue
G (Snapkick/Flip) 3-0 Blue
G (Jab/Uppercut) 4-0 Blue
G (Sweep/Fancy Duck) 4-1 Blue
G (Spinkick x2) 5-2 Blue
( ((Blue .def. JewellRavenlock, 5-2 in 6 rounds))


PIKACHU
1 ( Sweep / Jab ) 1 Grace
2 ( AB / Flip ) 2 Grace
3 ( Jab / F Dodge ) 3 Grace
4 ( Snap / Spin ) 1-3 Grace
5 ( Duck / Jab ) +1-3 Grace
6 ( Sweep / Jumpkick ) 1-3 Grace
7 ( Flip / Fdodge ) 1-4 Grace
F ( Jab x2 )
( ( Awkward .def. Ekthbjlgke, 5-2 in 8 DOF )


MI
1 (LC/CP) .5 - 0 Eri
2 (HC/TH) 1.5 - 0 Eri
3 (LC/CP) 2 - 0 Eri
4 (FCP/SH) 2 - 0 Eri
5 (FSS/TH) 2 - 1 Eri
6 (LC/CP) 2.5 - 1 Eri
7 (FSS/SS) 2.5 - 1 Eri
8 (HC/LC) 3.5 - 2 Eri
9 (SHx2) 3.5 - 2 Eri
6 (SL/CP) 3.5 - 3 Eri
1 (FSS/TH) 4 - 3.5 E
1 (DU/LP) 4.5 - 3.5 E
1 (LC/SL) 5.5 - 3.5 E
( (( FINAL: Ekthbjlgke .def. Sukeban, 5.5 - 3.5 in 13. ))


Back
D (Sweep/Chop) 1-0 Apple
D (Jab/Snapkick) 2-0 Apple
D (Sweep/Uppercut) 3-0 Apple
D (Snapkick/Flip) 4-0 Apple
D (Jab x2) 5-1 Apple
( ((Apple .def. eternal quest, 5-1 in 5 rounds))


Mighty
M (Flip/Jab) 1-0 Scout
M (Jumpkick/Snapkick) 1 up!
M (Flip/Jab) 2-1 Mr. Collins
M (Arm Block/Snapkick) 2-1+ Mr. Collins
M (Jab/Sweep) 3-1 Mr. Collins
M (Snapkick/Sweep) 3-2 Mr. Collins
M (Snapkick/Sweep) 4-2 Mr. Collins
M (Jab x2) 5-4 Mr. Collins
( ((Mister Collins .def. Scout Moran, 5-3 in 8 rounds))


Lindzei
1 (JB/JB) 1 All
2 (JK/LS) 1 All
3 (CH/HK) 2 All
4 (JB/DO) 2-2+ Gothrak
5 (CH/SN) 3-2 Blue
6 (FDU/JB) 4-2 Blue
7 (FUC/LS) 4-3 Blue
8 (FDU (Mentor Mod)/JK)
( ((Final: Blue .def. Gothrak, 5-3 in 8. DoF))


It's
S (Sweep/Jab, 5-4 Sandy)
( ((Final: Sandalio del Saenz .def. Munchem, 5-4 in 10 rounds, DoF))


Boogie
M (Jab/Sweep) 1-0 Blue
M (Spinkick/Fancy Dodge) 1 up!
M (Flip/Jab) 2-1 Blue
M (Snapkick x2) 3-2 Blue
M (Fancy Duck/Jab) 4-2 Blue
M (Spinkick/Arm Block) 5-2 Blue
( ((Blue .def. Izumi Takamine, 5-2 in 6 rounds))


Black
W (FDO/SP) 1 Sal
W (LS/JB) 2 Sal
W (JK/CH) 1-2 Sal
W (LS/LS) 2-3 Sal
W (SP/DO) 3 All
W (SN/JB) 3-4 Sal
W (JK/UC)
( ((Final: Delahada .def. Awkward, 5-3 in 7. DoF))


Animal
A (Sweep/Flip) 1-0 Munchem
A (Fancy Dodge/Dodge) still 1-0 Munchem
A (Jab/Snapkick) 1 up!
A (Dodge/Jab) 1+-1 Sal
A (Sweep x2) 2 up!
A (Spinkick x2) 3 all!
A (Sweep/Flip) 4-3 Munchem
A (Fancy Dodge/Jab) 5-3 Munchem
( ((Munchem .def. Delahada, 5-3 in 8 rounds))


Eden
1 (SH/WB) 1 Warren
2 (WB/AR) 1.5 Warren
3 (FT/MB) 1-1.5 Warren
4 (MB/WB) 1.5 All
5 (MS/FT) 1.5-2.5 Warren
6 (MW/MW) 1.5-2.5 Warren
7 (FT/DP) 1.5-3 Warren
8 (FF/MS) 2-3 Warren
9 (MW/MW) 2-3 Warren
1 (MB/WB) 2.5-3 Warren
1 (MW/FF) 2.5-3.5 Warren
1 (FT/MB) 3.5 All
1 (RF/FT) 3.5-4.5 Warren
1 (MB/AB) 4.5 All
1 (MS/DP)
( ((Final: eternal quest .ties. Elaine Aqua, 5-4.5 in 15. DoM))


DECAPRE
1 ( FCP / CP )
2 ( TH / HC ) 1 Mel
3 ( FCP / SL ) 1-1
4 ( TH x2 ) 2-2
5 ( FDU / HC ) 3-2 Mel
6 ( CP / SH ) 3-2 MEl
7 ( TH / LC ) 4-2 Mel
F ( FLP / TH )
( ( Fourth .def. Kruger, 5.0 - 2.0 in 8 )


RED
1 (JB/LS) 1-0 Grace!
2 (JK/HK) 2-0 Grace
3 (LS/DO) 3-0 Grace
4 (FDU/FL) 4-0 Grace
1 (LS/JB) 4-1 Grace
6 (JK/LS) 4-1 Grace


Kujata
1 (CH/LS) 1 Kyra
2 (SN/JK) 2 Kyra
3 (JB/LS) 1-2 Kyra
4 (UC/FL) 2 All
5 (CH/JB) 3 All
6 (LS/SN) 3-4 Kyra
7 (HK/FL)
( ((Final: Kyra Jessup .def. eternal quest, 5-3 in 7. DoF))


GOLD
1 (LS/LS) 1 all!
2 (FL/CH) 2-1 Warren
3 (CH/JB) 3-2 Warren!
4 (DO/LS) 3 all!
5 (SN/AB) 3+-3 Kyra
6 (HK/DO) 4-3 Warren
7 (JB/LS) 5-3 Warren
( (FINAL: eternal quest .def. Kyra Jessup, 5-3 in 7, DOF)


Civilization!
O (Flip/Jab) 1-0 Mr. Collins
T (Sweep x2) 2-1 Mr. Collins
T (Fancy Dodge/Flip) 2 up!
F (Jab/Sweep) 3-2 Mr. Collins
F (Jumpkick x2) Still 3-2 Mr. Collins
S (Jab/Snapkick) 3 up!
S (Jumpkick/Sweep) Still 3 all
E (Snapkick/Flip) 4-3 Mr. Collins
N (Jab/Sweep) 4 all!
T (Jab/Chop) 5 up!
E (Jumpkick/Chop) 6-5 Munchem
( ((Munchem .def. Mister Collins, 6-5 in 11 rounds))


Vulture
1 (Jab / Jab. 1-1.)
2 (Jumpkick / Jumpkick. 1-1.)
3 (Snapkick / Sweep. 2-1, Kruger.)
4 (Jab / Chop. 3-2, Kruger.)
5 (Fancy Dodge / Snapkick. 4-2, Kruger.)
F (Jab / Jab. 5-3, Kruger.)
( (Kruger .def. Awkward, 5-3 in 6, Dof.**)


Etro
1 (DO/FDO)
2 (SN/LS) 1 Collins
3 (LS/FL) 2 Collins
4 (CH/FAB) 2-1 Collins
5 (JB/JB) 3-2 Collins
6 (LS/JK) 3-2 Collins
7 (JB/FL) 3 All
8 (JK/LS) 3 All
9 (JB/JK) 4-3 Collins
1 (CH/SN)
( ((Final: Mister Collins .def. Munchem, 5-3 in 10. DoF))


MEOWTH
1 ( Jab / Flip ) 1 Kyra
2 ( Flip / Jab ) 1-1
3 ( Snap / Hook ) 2-1 Crunchem
4 ( Sweep / Leg Block ) 2-+1 Crunchem
5 ( Dodge / Duck ) 2-1 Crunchem
6 ( Flip / Dodge ) 2-+1 Crunchem
7 ( AB / Leap ) 2-1 Crunchem
8 ( Jab / Snap ) 3-1 Crunchem
9 ( Flip / Sweep ) 3-2 Crunchem
1 ( Jab / Jab ) 4-3 Crunchem!
F ( Jumpkick / Chop )
( ( Crunchem .def. Kyra Jessup, 5-3 in 11 DOF )


B
M (Sweep x2, 1 all)
S (Jab/FA Dodge, 2-1 Gren)
O (Flip/Jab, 2 all)
C (FA Leap/Flip, 3-2 Mel)
K (Sweep/Jab, 3 all)
M (FE Jumpkick/Sweep, 3 all)
M (Jab/Snapkick, 4-3 Mel)
G (Sweep/FA Leap, 4 all)
M (FA Leap/Flip, 5-4 Mel)
( ((Final: Fourth .def. Gren Blockman, 5-4 in 9 rounds, DoF))


Bartender
1 (JB/JB) 1 All
2 (JK/JK) 1 All
3 (LS/JB) 1-2 Crunchem
4 (JK/FL) 1-3 Crunchem
5 (FDO/LS) 1-4 Crunchem
6 (FAB/FL)
( ((Final: Crunchem .def. Awkward, 5-1 in 6. DoF))


F
W (Snapkick/Jab, 1-0 Munch)
N (Jab/Flip, 2-0 Munch)
W (Duck/Sweep, 3-0 Munch)
W (Flip/FA Dodge, 4-0 Munch)
O (Sweep/Jab, 5-0 Munch)
( ((Final: Munchem .def. GiftGiver, 5-0 in 5, DoF))


J
O (Dodge/Sweep, 1-0 Munch)
I (Armblock/Jab, 1-0+ Munch)
O (Jab/Jumpkick, 1 all)
I (Legblock/Jab, 2-1 Munch)
O (Sweep/Snapkick, 3-1 Munch)
B (Jab/FA Dodge, 4-1 Munch*
O (Flip/FA Leap, 5-1 Munch)
( ((Final: Munchem .def. Mark Stuart, 5-1 in 7 rounds, DoF))


Disco
I (Flip/Jab) 1-0 Grace
I (Sweep/Chop) 2-0 Grace
I (Fancy Dodge/Chop) 2-1 Grace
I (Jab/Sweep) 3-1 Grace
I (Fancy Dodge/Flip) 4-1 Grace
I (Chop/Fancy Duck) 5-1 Grace
( ((Awkward .def. Munchem, 5-1 in 6 rounds))


Leviathan
1 (DO/FL) +-0 Gothrak
2 (CH/CH) 1 All
3 (JB/FDO) 1-2 Shale
4 (SN/FL) 2 All
5 (JB/JB) 3 All
6 (SN/LS) 4-3 Gothrak
7 (FL/FDU) 4 All
8 (LS/JB)
( ((Final: Shale .def. Gothrak, 5-4 in 8. DoF))


Piggy
B (Flip x2) Nada
B (Jab/Sweep) 1-0 Scout
B (Chop/Snapkick) 2-0 Scout
B (Jab/Sweep) 3-0 Scout
B (Snapkick/Flip) 3-1 Scout
B (Jab/Jumpkick) 4-1 Scout
B (Snapkick/Sweep) 5-1 Scout
( ((Scout Moran .def. Mister Collins, 5-1 in 7 rounds))


Ultra
1 (Jab / Jumpkick. 1-0, Mister Collins.)
2 (Snapkick / Sweep. 2-0, Mister Collins.)
3 (Jab / Uppercut. 3-1, Mister Collins.)
4 (Flip / Hook. 4-0, Mister Collins.)
F (Snapkick / Spinkick. 5-0, Mister Collins.)
( (Mister Collins .def. Eternal Quest, 5-0 in 5, DoF.**)


BLUE
E ( Jab / Flip ) 1 Shale
E ( Snapkick x2 ) 1-2 Shale
E ( Flip / Sweep ) 1-3 Shale
E ( Jab x2 ) 2-4 Shale
E ( Sweep / Snapkick )
( ( Shale .def. Zynn, 5-2 in 5 DOF )
E ( Dodge / Flip ) +0 Gothrak
E ( Duck / Jab ) 1 Gothrak
E ( Snap / Sweep ) 2 Gothrak
E ( Jumpkick / Dodge ) 2-+0 Gothrak
E ( Sweep / Flip ) 3 Gothrak
E ( Jab x2 ) 4-1 Gothrak
E ( Chop / Sweep ) 4-2 Gothrak
E ( Jab x2 )
( ( Gothrak .def. Zynn, 5-3 in 8 DOF )


Outta
C (Jab x2) 1 up!
C (Jumpkick/Sweep) Still 1 up
C (Jab x2) 2 up!
C (Fancy Leap/Snapkick) 3-2 Blue
C (Feint Flip/Fancy Dodge) 4-2 Blue
C (Uppercut/Flip) 5-2 Blue
( ((Blue .def. Red urThorne, 5-2 in 6 rounds))


Titan
1 (Dodge / Jab. 0+-0, Melody.)
2 (Sweep / Arm Block. 1-0, Red.)
3 (Sweep / Fancy Dodge. 1-1.)
4 (Snapkick / Flip. 2-1, Melody.)
5 (Fancy Dodge / Flip. 2-2.)
6 (Arm Block / Jab. 2+-2, Melody.)
7 (Jumpkick / Jumpkick. 2-2.)
8 (Jab / Snapkick. 3-2, Melody.)
9 (Snapkick / Sweep.4-2, Melody.)
F (Jab / Jab. 5-3, Melody.)
( (Melody Swan .def. Red urThorne, 5-3 in 10, DoF.)


POISON
1 ( LC / TH ) 1 Charlie
2 ( Cp / LC ) .5-1 Charlie
3 ( LP / SS ) .5-1.5 Charlie
4 ( TH / CP ) .5-2 Charlie
5 ( LC / HC ) 1.5-3 Charlie
6 ( SL / LC ) 1.5-4 Charlie
F ( FSS / HC )
( ( Resolute .def. JC, 5.0 - 1.5 in 7 )


Walk
t (JB/LS) 1 Warren
t (HK/SN) 1 All
t (LS/LS) 2 All
t (CH/JB) 3 All
t (SN/FDU) 4-3 Warren
t (UC/LS) 4 All
t (JB/JB) 5 All
t (HK/JK)
( ((Final: Blue .def. eternal quest, 6-5 in 8. DoF))


Chimera
1 (Sweep / Chop. 1-0 E.)
2 (Jab / Sweep. 2-0, E.)
3 (Arm Block / Snapkick. 2+-0, E.)
4 (Jab / Jab. 3-1, E.)
5 (Jumpkick / Snapkick. 4-1, E.)
F (Flip / Jab. 5-1, E.)
( (Ekthbjlgke .def. Kyra Jessup, 5-1 in 6, DoF.**)


TRAP
C ( Jab / Snap ) 1 Grace
C ( FLip / Uppercut ) 1-1
C ( Jab / Sweep ) 2-1 Grace
C ( F Dodge / Chop ) 3-1 Grace
C ( Chop / Sweep ) 3-2 Grace
C ( Jab / Jumpkick ) 4-2 Grace
C ( Jumpkick / Hook )
( ( Awkward .def. eternal quest, 5-2 in 7 DOF )


Crisis
1 (Flip / Jab. 1-0, Addie.)
2 (Snapkick / Sweep. 1-1.)
3 (Flip / Jab. 2-1, Addie.)
4 (Flip / Jumpkick. 2-2.)
5 (Arm Block / Jab. 2+-2, Addie.)
6 (Arm Block / Leap. 2-2.)
7 (Sweep / Dodge. 3-2, E.)
( ( <a href="https://www.youtube.com/watch?v=gH476CxJxfg" target="_blank" style="color:#660066">https://www.youtube.com/watch?v=gH476CxJxfg</a> ) "Where is the moment we needed the most? You kick up the leaves and the magic is lost. They tell me your blue skies faded to grey. They tell me your passion's gone awayAnd I don't need no carrying on" Perhaps a little bit of sympathy for Amanda... despite all the shoving. Next thing she'd pull a Peaches move and start with the smushing!
8 (Spinkick / Dodge. 3-3.)
9 (Sweep / Flip. 4-3, Addie.)
1 (Duck / Hook. 4-3+, Addie.)
F (Jab / Snapkick. 5-3, Addie.)
( (Addie Alcar .def. Ekthbjlgke, 5-3 in in 11, DoF.**)


Crunchem
2 (LS/LS) 2 All


DO
1 (FSS/FCP) 1 - 0 Mel
2 (HC/LC) 2 - 1 Mel
3 (TH/HC) 3 - 1
4 (LC/FDU) 4 - 1 Mel
5 (FLPx2) 4 - 1 Mel
6 (SH/HC) 4 - 2 Mel
7 (LC/FLP) 5 - 2 Mel
( (( FINAL: Fourth .def. Ticallion Carter, 5 - 2 in 7. ))


Easy
L (Mage Bolt/Wizard Blades) 0.5-0 Melanie
L (Displacement/Reflection) Still 0.5-0 Melanie
L (Mage Bolt/Wizard Blades) 1-0 Melanie
L (Fear Touch/Foul Fog) 1 up!
L (Shield/Mage Bolt) 1.5-1 Misery
L (Ghostform/Armor) 2-1 Misery
L (Reflection/Focused Shield) 2.5-1 Misery
L (Mage Bolt/Wizard Blades) 3-1 Misery
L (Displacement/Wizard Blades) 3-1.5 Misery
L (Foul Fog/Mind Whip) 3-2 Misery
L (Fear Touch/Mage Bolt) 3 all!
L (Wizard Blades/Meteor Shower) 3.5-3 Melanie
L (Ghostform/Reflection) Still 3.5-3 Melanie
L (Mind Whip/Fear Touch) 4.5-3 Melanie
L (Arctic Blast/Armor) 5.5-3 Melanie
( ((Fourth .def. Flowing Tears, 5.5-3 in 15 rounds))


Heat!
O (Jab/Chop) 1 up!
T (Snapkick x2) 2 all!
T (Sweep/Fancy Duck) 3-2 Mister Collins
F (Sweep/Spinkick) 3 all!
F (Jab x2) 4 up!
S (Jumpkick x2) Still 4 all
S (Jab x2) 5 all!
E (Chop/Uppercut) 6-5 Mister Collins
( ((Mister Collins .def. Awkward, 6-5 in 8 rounds))


YELLOW
2 (JB/JB) 2-1 Warren
3 (UC/JK) 3-1 Warren!
4 (AB/JB) 3+-1 Warren
5 (HK/DO) 4-1 Warren
6 (JK/LS) 4-1 Warren
6 (SP/AB) 5-1 Warren
( (FINAL: eternal quest .def. The Boy, 5-1 in 7, DOF)


GREEN
1 (JK/SP) 1-0 Atreyu
2 (CH/LP) 2-0 Chop!
3 (SP/JB) 2-1 Atreyu!
4 (FL/SN) 2 all
5 (UC/JB) 3 Kyo!
6 (HK/JK) 4-3 Kyo
7 (JB/LS) 4-3 Kyo
8 (LS/SN) 5-4 Kyo
( (FINAL: Kyo Mori .def. Atreyu Carter, 5-3 in 8, DOF)


Bounce
1 (LS/JB) 1 Munchem
2 (SN/CH) 2 Munchem
3 (JB/LS) 1-2 Munchem
4 (FL/SN) 1-3 Munchem
5 (DU/FDO) 1-3 Munchem
6 (JB/FL) 1-4 Munchem
7 (AB/FDO) 1-4 Munchem
8 (FL/UC)
( ((Final: Munchem .def. Ekthbjlgke, 5-1 in 8. DoF))


Jingle
J (Jab/Hook) 1-0 Blue
J (Snapkick/Uppercut) 2-0 Blue
J (Fancy Dodge/Snapkick) 3-0 Blue
J (Flip x2) Still 3-0 Blue
J (Jab x2) 4-1 Blue
J (Fancy Uppercut/Jumpkick) 5-1 Blue
( ((Blue .def. Addie, 5-1 in 6 rounds))


FIJI
1 (LS/HK) 1-0 Blue!
2 (FL/AB) 2-0 Blue!
3 (FDO/FL) 3-0 Blue
4 (JB/DO) 3-0+ Blue!
5 (CH/LS) 3-1 Blue
6 (JB/FL) 3-2 Blue
7 (SN/SN) 4-3 Blue!
8 (FL/DO) 4-3+ Blue
9 (FtJK/AB) 5-3 Blue


Phoenix
1 (SH/DP) .5 Greenlee
2 (WB/FF) 1-.5 Mel
3 (GF/WB) 1.5-.5 Melanie
4 (MB/MW) 2.5-.5 Melanie
5 (FF/AB) 3.5-.5 Melanie
6 (RF/RF) 4.5-1.5 Melanie
7 (SH/MS) 4.5-2.5 Melanie
8 (MB/WB)
( ((Final: Fourth .def. Greenlee, 5-2.5 in 8. DoM))


Let's
G (Sweep/Chop) 1-0 Kyra
G (Uppercut/Flip) 1 all!
G (Jab/Sweep) 2-1 Kyra
G (Dodge/Snapkick) 2+-1 Kyra
G (Arm Block/Jumpkick) 3-1 Kyra
G (Jab x2) 4-2 Kyra
G (Fancy Arm Block/Hook) 5-2 Kyra
( ((Kyra Jessup .def. eternal quest, 5-2 in 7 rounds))


Blood
H (Fancy Dodge/Jab) 1-0 Jewell
H (Jab/Snapkick) 2-0 Jewell
H (Jumpkick/Sweep) Still 2-0 Jewell
H (Chop/Snapkick) 2-1 Jewell
H (Jab x2) 3-2 Jewell
H (Fancy Arm Block/Jumpkick) 4-2 Jewell
H (Sweep/Flip) 4-3 Jewell
H (Sweep/Flip) 5-3 Jewell
( ((JewellRavenlock .def. Mister Collins, 5-3 in 8 rounds))


Many
M (Sweep/Chop) 1-0 Scout
M (Flip x2) Still 1-0 Scout
M (Jab x2) 2-1 Scout
M (Jumpkick/Leg Block) 3-1 Scout
M (Jab x2) 4-2 Scout
M (Jumpkick/Snapkick) 5-2 Scout
( ((Scout Moran .def. Kyra Jessup, 5-2 in 6 rounds))


A
S (Flip/FA Dodge, 1-0 Jewell)
D (Dodge/Sweep, 2-0 Jewell)
S (Duck/Jab, 2-0+ Jewell)
V (Sweep x2, 3-1 Jewell)
W (Dodge/FA Armblock, 3-1 Jewell)
S (Jab/Flip, 4-1 Jewell)
I (Snapkick/Uppercut, 4-2 Jewell)
U (Jab/Sweep, 4-3 Jewell)
J (Chop/Spinkick, 4 all) SUDDEN DEATH!!!
I (Uppercut/Jab, 5-4 Jewell)
( ((Final: JewellRavenlock .def. Delahada, 5-4 in 10 rounds, DoF))


E
S (Spinkick/Sweep, 1-0 Kyra)
* (Armblock/Duck, 1-0 Kyra)
K (Snapkick/Jab, 2-0 Kyra)
O (Sweep/Armblock, 2-1 Kyra)
D (Jab/FA Dodge, 3-1 Kyra)
S (Chop/Snapkick, 3-2 Kyra)
K (Jab/Flip, 4-2 Kyra)
A (Jumpkick/Jab, 5-2 Kyra)
( ((Final: Kyra Jessup .def. Delahada, 5-2 in 8 rounds, DoF))


I
B (FA Dodge/Jab, 1-0 Blue)
P (Sweep/Flip, 2-0 Blue)
M (Jab x2, 3-1 Blue)
S (Snapkick/Chop, 3-2 Blue)
I (FA Dodge/Sweep, 3 all)
M (Jab x2, 4 all)
S (Jumpkick/Sweep, 4 all)
A (Flip x2, 4 all)
M (Jab/Chop, 5 all)
S (Snapkick/Sweep, 6-5 Blue)
( ((Final: Blue .def. GiftGiver, 6-5 in 10 rounds, DoF))


Pillar
1 (Jab / Sweep. 1-0, Blue.)
4 (Sweep / Uppercut. 3-1, Munchem.)
2 (Dodge / Chop. 1-0+, Blue.)
3 (Duck / Flip. 1-1.)
4 (Sweep / Sweep. 2-2.)
5 (Spinkick / Flip. 3-2, Blue.)
6 (Jab / Jab. 4-3, Blue.)
7 (Jumpkick / Jumpkick. 4-3, Blue.)
F (Spinkick / Dodge. 5-3, Blue.)
( (Light Up The Night .def. Ekthbjlgke, 5-3 in 8, DOF.**)


CIDER
2 (LS/FDO) 2-1 Kyra
3 (AB/FL) 2 all!
4 (JB/JB) 3 all!
5 (LS/JK) 3 all
6 (SN/LS) 4-3 Kyra
7 (CH/FDO) 4 all
8 (FL/FaLP) 5-4 Red!
( (FINAL: Red urThorne .def. Kyra Jessup, 5-4 in 8, DOF)


BROOT
1 (JB/LS) 1-0 Gren
2 (FL/SP) 1 all!
3 (JB/LS) 2-1 Gren!
4 (FL/HK) 3-1 Gren
5 (FDO/JB) 4-1 Gren
9 (LS/FL) 5-1 Gren
( (FINAL: Gren Blockman .def. Ticallion Carter, 5-1 in 6, DOF)


PINK
1 (JB/LS) 1-0 Kyra
2 (DO/SN) 1+-0 Kyra!
3 (JB/LS) 2-0 Kyra
4 (FL/JB) 3-0 Kyra
5 (LS/SP) 4-0 Kyra
6 (JB/FDO) 4-1 Kyra
7 (JK/SN) 5-1 Kyra
( (FINAL: Kyra Jessup .def. JewellRavenlock, 5-1 in 7, DOF)


Devilfish
1 (Jab / Sweep. 1-0, Munchem.)
2 (Snapkick / Sweep. 1-1.)
3 (Fancy Dodge / Jab. 2-1, Munchem.)
5 (Fancy Duck / Jab. 4-1, Munchem.)
6 (Dodge / Uppercut. 4-1+, Munchem.)
7 (Legblock / Sweep. 4-2, Munchem.)
F (Jab / Hook. 5-2, Munchem.)
( (Munchem .def. Eternal Quest, 5-2 in 8, DoF**)


Yeah
1 (DO/B) +-0 E
2 (LS/DO) 1 E
3 (JB/AB) 1-+ E
4 (AB/SN) 1+-0 E
5 (JB/DO) 1-+ E
6 (FL/LS) 1 All
7 (JB/DO) 1-1+ Sal
8 (CH/JB) 2 All
9 (JK/SN) 3-2 E
1 (FL/JB) 4-2 E
1 (SN/DO) 4-2+ E
1 (LS/SP)
( ((Final: Ekthbjlgke .def. Delahada, 5-2 in 12. DoF))


Fancy
1 (JB/JB) 1 All
3 (SN/JB) 2 All
4 (JK/FL) 2-3 Skid
5 (JB/LS) 3 All
6 (CH/JB) 4 ALl
7 (LS/UC)
( ((Final: Mister Collins .def. Necromesh, 5-4 in 7. DoF))


Superheavy
1 (Fancy Dodge / Jab. 1-0, Munchem.)
2 (Flip / Jumpkick. 2-0, Munchem.)
3 (Jab / Jab. 3-1, Munchem.)
4 (Jumpkick / Sweep. 3-1, Munchem.)
5 (Jab / Jab. 4-2, Munchem.)
F (Sweep / Sweep. 5-3, Munchem.)


TART
1 (JB/JB) 1 all
2 (JK/UC) 2-1 Boy
3 (JB/JB) 3-2 Boy
4 (CH/LS) 4-2 Boy
5 (FDU/SP) 4-3 Boy
6 (FDO/LS) 5-3 Boy
( (FINAL: The Boy .def. Kyo Mori, 5-3 in 6, DOF)


