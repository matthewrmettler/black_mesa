import operator

filename = "data/match_history.txt"

columns = ["Date", "Caller", "Duelist 1","Result","Duelist 2","Score1","Score2", "Rounds"]
match = []

winner_raw = []
duelists = {}
lost = {}
def most_won():
	global duelists
	for m in match:
		winner_raw.append(m[2])
	for win in winner_raw:
		if win in duelists:
			duelists[win] += 1
		else:
			duelists[win] = 1
	duelists = sorted(duelists.iteritems(), key=operator.itemgetter(1), reverse = True)

def most_loss():
	global lost
	lost_raw = []
	for m in match:
		lost_raw.append(m[4])
	for l in lost_raw:
		if l in lost:
			lost[l] += 1
		else:
			lost[l] = 1
	lost = sorted(lost.iteritems(), key=operator.itemgetter(1), reverse = True)

def main():
	with open(filename, 'r') as f:
		for line in f:
			temp = line.split("\t")
			temp = [x[:-1] for x in temp]
			match.append(temp)
main()
for m in match:
	print(m)
most_won()
most_loss()
#print(duelists)
print(lost)