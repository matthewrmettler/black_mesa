import re

input_file = "data/duels4.txt"

dos_moves_filter = ["TH", "LC", "HC", "SL", "SH", "CP", "LP", "SS", "DI"]
dos_prefix = ["F"]

dof_moves_filter = ["Jab", "JB", "Chop", "CH", "Hook", "HK", "Uppercut", "UC", "Flip", "FL", "Jumpkick", "JK", "Snapkick", "SNK", "SK", "Legsweep", "LS", "SW", "Spinkick", "SpK", "SP", "Armblock", "AB", "Legblock", "LB", "Dodge", "Duck", "Leap", "LP"]
dof_prefix = ["F", "FE", "Fe"]

dris_rings = {}
amanda_rings = {}
rings = {}

last_ring = ""

def hasNumbers(inputString):
    return any(char.isdigit() for char in inputString)

def addToRing(caller, s):
	global last_ring
	#print(s)
	ring = caller + " " + s[0]
	if hasNumbers(ring):
		ring = caller + " " + s[0][0]
	if "(" in ring: #final score declaration
		ring = last_ring
		round = ' '.join(s)
		if ring in rings:
			rings[ring].append(round)
		else:
			rings[ring] = []
			rings[ring].append(round)
	else:
		round = ' '.join(s[1:])
		if ring in rings:
			rings[ring].append(round)
		else:
			rings[ring] = []
			rings[ring].append(round)
		last_ring = ring

def addLine(caller, sentence):
	if "/" in sentence or "x2" in sentence or "def" in sentence or ".def." in sentence or "final" in sentence.lower() or "(final" in sentence.lower() or "((final" in sentence.lower():
		addToRing(caller, sentence.split(' '))

def printRoundScores():
	#print(rings.keys())
	for key in rings.keys():
		print(key)
		printScores(rings[key])
def printScores(ring):
	for round in ring:
		if (len(round) < 1): continue
		if "(" in round:
			print(round[0] + " " + round[round.index('('):-1])

def writeRoundScores():
	#print(rings)
	for key in rings.keys():
		with open("match/" + re.sub(r'[^\w]', ' ', key) + ".txt", 'a+') as f:
			#print(f)
			f.write(key + "\n")
			writeScores(f, rings[key])

def writeScores(file, ring):
	for round in ring:
		if (len(round) < 1): continue
		if "(" in round:
			file.write(round[0] + " " + round[round.index('('):-1]+ "\n")

def main():
	with open(input_file, 'r') as f:
		for l in f:
			line = l.split('\t')
			if (len(line) > 1): 
				addLine(line[0].split(' ')[1], line[1])

main()
writeRoundScores()

#NOTES
#GALAXY fucked up, do manually?
#The Boy def Munchem, youtube links, all fucked up
#( (FINAL: Awkward .def. Ekthbjlgke, 4-1 in 6 (forfeit), DOF) -- found in wrong file
#( ((Final: Kyra Jessup .def. Zynn, 5-3 in 7. DoF)) -- found in wrong file