import glob

match = "match/Claire/Claire Siren.txt"

move_names = ["JB", "HK", "UC", "CH", "JK", "SP", "SK", "LS", "FL", "AB", "LB", "DO", "DU", "LP"]
fancy_move_names = ["F"+move for move in move_names]
matrix = [	[2, 0, 0, 2, 0, 0, 0, 0, 1, 5, 0, 5, 5, 0],
			[1, 2, 1, 2, 1, 0, 1, 1, 1, 5, 0, 0, 5, 0],
			[1, 0, 2, 1, 0, 0, 1, 1, 0, 5, 0, 5, 5, 0],
			[2, 2, 0, 2, 1, 0, 0, 1, 1, 5, 0, 5, 0, 0],
			[1, 0, 1, 0, 3, 0, 0, 3, 1, 5, 0, 5, 5, 0],
			[1, 1, 1, 1, 1, 2, 1, 1, 0, 0, 0, 0, 5, 0],
			[1, 0, 0, 1, 1, 0, 2, 0, 0, 5, 5, 5, 0, 5],
			[1, 0, 0, 0, 3, 0, 1, 2, 0, 0, 5, 0, 0, 5],
			[0, 0, 1, 0, 0, 1, 1, 1, 3, 0, 3, 5, 5, 5],
			[4, 4, 4, 4, 4, 1, 4, 1, 1, 3, 3, 3, 3, 3],
			[1, 1, 1, 1, 1, 1, 4, 4, 3, 3, 3, 3, 3, 3],
			[4, 1, 4, 4, 4, 1, 4, 1, 4, 3, 3, 3, 3, 3],
			[4, 4, 4, 1, 4, 4, 1, 1, 4, 3, 3, 3, 3, 3],
			[1, 1, 1, 1, 1, 1, 4, 4, 4, 3, 3, 3, 3, 3]]

#0: row won
#1: column won
#2: both scored
#3: neither scored
#4: row adv
#5: col adv

last_line = ""
winning_moves = []
losing_moves = []
round_winner = []
round_loser = []

winner = ""
loser = ""
moves = []
leaders = []

winner_moves = []
winner_score = []
loser_moves = []
loser_score = []

round_results = []
round_score = []

def getDuelists(l):
	global winner
	global loser
	line = l.split(' ')
	i = line.index('.def.')
	winner = line[i-1]
	loser = line[i+1][:-1]

def parseMove(move):
	moves = move[1:-1].split('/')
	return moves

def getResults(moves):
	for round in moves:
		row = round[0]
		col = round[1]
		if not row in move_names: #fancy
			if not row in fancy_move_names: #somethings wrong
				pass
			else:
				row_i = fancy_move_names.index(row)
		else:
			row_i = move_names.index(row)
		if not col in move_names:
			if not col in fancy_move_names:
				pass
			else:
				col_i = fancy_move_names.index(col)
		else:
			col_i = move_names.index(col)
		result = matrix[row_i][col_i]
		round_results.append(result)
		if result == 0  or result == 4: #row won
			winning_moves.append(row)
			losing_moves.append(col)
		else: #col won, or same move
			winning_moves.append(col)
			losing_moves.append(row)

def getRoundWinner():
	winner_score.insert(0, "0")
	loser_score.insert(0, "0")
	match_winner_diff = []
	match_loser_diff = []
	for x in range(1, len(winner_score)):
		match_winner_diff.append(int(winner_score[x])-int(winner_score[x-1]))
		match_loser_diff.append(int(loser_score[x])-int(loser_score[x-1]))
	for x in range(len(match_winner_diff)):
		if match_winner_diff[x] > match_loser_diff[x]:
			round_winner.append(winner)
			round_loser.append(loser)
		else:
			round_winner.append(loser)
			round_loser.append(winner)

def playerScores(scores, lead_name):
	global winner_score
	global loser_score
	for i in range(len(scores)-1, -1, -1):
		if not "-" in scores[i]: #tied score
			if not "all" == lead_name[i].lower():
				if lead_name[i].rstrip('\n') == winner:
					winner_score.append(scores[i])
					loser_score.append("0")
				else:
					winner_score.append("0")
					loser_score.append(scores[i])
			else: #is all, tied
				winner_score.append(scores[i])
				loser_score.append(scores[i])
		else:
			temp_score = scores[i].split("-")
			lead = max(temp_score[0], temp_score[1])
			not_lead = min(temp_score[0], temp_score[1])
			if lead_name[i].rstrip('\n') == winner:
				winner_score.append(lead)
				loser_score.append(not_lead)
			else:
				winner_score.append(not_lead)
				loser_score.append(lead)
	winner_score = winner_score[::-1]
	loser_score = loser_score[::-1]

def parseRound(line):
	round = line.split(' ')
	if len(round) != 4: return False
	round_num = round[0]
	round_score.append(round[2])
	moves.append(parseMove(round[1]))
	leaders.append(round[3])

def identify_rowcol():
	col_score = round_results.count(1) + round_results.count(2)
	row_score = round_results.count(0) + round_results.count(2)

def moves_for_players():
	global winner_moves
	global loser_move
	for x in range(len(round_winner)):
		if round_winner[x] == winner:
			winner_moves.append(winning_moves[x])
			loser_moves.append(losing_moves[x])
		else:
			winner_moves.append(losing_moves[x])
			loser_moves.append(winning_moves[x])

def createParsedMatch():
	caller = match.split("/")[1] + "/"
	with open("parsed_matches/" + caller + winner + " vs " + loser + ".txt", "w") as f:
		f.write(winner+"\n")
		f.write(loser+"\n")
		for x in range(len(winner_moves)):
			f.write(str(x+1) + " " + winner_moves[x] + " " + loser_moves[x] + " " + winner_score[x+1] + " " + loser_score[x+1] +"\n")

def main():
	print(match)
	with open(match, 'r') as f:
		for line in f:
			parseRound(line)
			last_line = line
	getDuelists(last_line)
	print(leaders)
	playerScores(round_score, leaders)
	identify_rowcol()
	getResults(moves)
	getRoundWinner()
	prints()
	moves_for_players()
	createParsedMatch()

def clear():
	global winning_moves
	global last_line
	global winning_moves
	global losing_moves
	global round_winner
	global round_loser
	global winner
	global loser
	global moves
	global leaders

	winner_moves = []
	winner_score = []
	loser_moves = []
	loser_score = []

	last_line = ""
	winning_moves = []
	losing_moves = []
	round_winner = []
	round_loser = []

	winner = ""
	loser = ""
	moves = []
	leaders = []

	winner_moves = []
	winner_score = []
	loser_moves = []
	loser_score = []

	round_results = []
	round_score = []

def prints():
	print(winner + " defeated " + loser)
	#print(round_score)
	#print(moves)
	#print(leaders)
	print(winner + ":")
	print(winner_score)
	print(loser + ":")
	print(loser_score)
	#print(round_results)
	print("")
	print("Losing moves:")
	print(losing_moves)

	print("Winning moves:")
	print(winning_moves)

	print("Round winner:")
	print(round_winner)
	#moves_for_players()
	#print("\nMatch winner: " + winner)
	#print("Moves by round:")
	#for x in range(len(winner_moves)):
		#print("Round " + str(x+1) + ": " + winner_moves[x])
	#print("\nMatch loser: " + loser)
	#print("Moves by round:")
	#for x in range(len(loser_moves)):
		#print("Round " + str(x+1) + ": " + loser_moves[x])


main()
